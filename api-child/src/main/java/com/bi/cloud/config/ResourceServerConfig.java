package com.bi.cloud.config;

import com.bi.cloud.handler.AuthExceptionEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;

import javax.annotation.Resource;
import javax.sql.DataSource;

@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Value("${security.oauth2.resource.id}")
    private String resourceId;

   @Autowired
   private AuthExceptionEntryPoint authExceptionEntryPoint;

   @Autowired
   private TokenStore jwtTokenStore;


   @Override
   public void configure(ResourceServerSecurityConfigurer resources) {
       resources.authenticationEntryPoint(authExceptionEntryPoint) // 认证失败时候调用
              // .tokenServices(tokenService())
               .resourceId(resourceId) // 必须与认证服务的resourceIds一致，不然就干脆都不配置，ResourceServerSecurityConfigurer默认一个
               .tokenStore(jwtTokenStore)
               .stateless(true);
   }
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()// 对相关请求进行授权
                .anyRequest()// 任意请求
                .authenticated()// 都要经过验证
                /*.and()
                .requestMatchers()// 设置要保护的资源
                .antMatchers("/**")*/;// 保护的资源
    }

    /**
     * 改用存缓存校验token，token的认证方式
     * @return
     */
   /* @Bean
    public DefaultTokenServices tokenService(){
        DefaultTokenServices tokenServices = new DefaultTokenServices();
        tokenServices.setSupportRefreshToken(true);
        tokenServices.setTokenStore(jwtTokenStore);
        return tokenServices;
    }*/

}
