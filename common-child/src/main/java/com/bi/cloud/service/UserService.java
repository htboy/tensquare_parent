package com.bi.cloud.service;

import com.bi.cloud.pojo.Users;

public interface UserService {
    Users userInfo(String username);
}
