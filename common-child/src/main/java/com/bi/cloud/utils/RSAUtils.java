package com.bi.cloud.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * 获取公钥
 */
public class RSAUtils {
    /**
     * 非对称加密公钥Key
     */
    public static String getPubKey(String pubPath) throws Exception {
        InputStream in   = null;
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        try {
            URL url = new URL(pubPath);
            URLConnection conn = url.openConnection();
            in = conn.getInputStream();
            reader = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            return  sb.toString();
        }catch (Exception e){
            throw new Exception("读取公钥失败");
        }finally {
            if(null != reader){
                reader.close();
            }
            if(null != in){
                in.close();
            }
        }
    }
}
