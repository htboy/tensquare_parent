package com.bi.cloud.filter;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;



/**
 * 做全局过滤
 * https://cloud.spring.io/spring-cloud-gateway/reference/html/#gateway-combined-global-filter-and-gatewayfilter-ordering
 */
@Component
public class AuthorizeFilter implements GlobalFilter, Ordered {
    private static final String AUTHORIZE_TOKEN = "Authorization";
    private static final String [] paths = {"/oauth/authorize","/login","/oauth/token","/code"};

    private  final  static Logger logger = LoggerFactory.getLogger(AuthorizeFilter. class);
    @Resource
    private TokenStore tokenStore;


    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //1. 获取请求
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse  response = exchange.getResponse();
        //2. 获取请求路径
        String requestUrl = exchange.getRequest().getPath().value();
        //3. 放行路径
        boolean pass = false;
        for(int i=0;i<paths.length;i++){
            if(requestUrl.contains(paths[i])){
                pass = true;
            }
        }
        if (pass) {
            return chain.filter(exchange);
        }
        //4. 获取请求头
        HttpHeaders headers = request.getHeaders();
        //5. 请求头中获取令牌
        String tokenStr = headers.getFirst(AUTHORIZE_TOKEN);

        //6. 判断请求头中是否有令牌
        if (StringUtils.isEmpty(tokenStr)) {
            tokenStr = request.getQueryParams().getFirst("access_token");
        }

        if (StringUtils.isEmpty(tokenStr)) {

            //7. 响应中放入返回的状态吗, 没有权限访问
            response.setStatusCode(HttpStatus.NON_AUTHORITATIVE_INFORMATION);
            logger.info("Authorization:{}:","Authorization 为空");
            return  errorBackCall(response,HttpStatus.NON_AUTHORITATIVE_INFORMATION.value(),"无效的Authorization");
        }

        String token = tokenStr.split(" ")[1];
        //9. 判断请求头中是否有令牌
        if (StringUtils.isEmpty(token)) {
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            logger.info("token{}:","token 为空");
            return  errorBackCall(response,HttpStatus.UNAUTHORIZED.value(), "无效的token");
        }
        //9. token存在 判断token是否有效
        OAuth2AccessToken oAuth2AccessToken = tokenStore.readAccessToken(token);
        int n = oAuth2AccessToken.getExpiresIn();
        if(n < 0 || n ==0){
            //10. token已经超时
            response.setStatusCode(HttpStatus.REQUEST_TIMEOUT);
            logger.info("token{}:","token已过期");
            return  errorBackCall(response,HttpStatus.REQUEST_TIMEOUT.value(),"token已过期");
        }
        /*Map<String, Object> map = oAuth2AccessToken.getAdditionalInformation();
        //取出用户身份信息
        String principal = (String) map.get("username");
        //获取用户权限
        List<String> authorities = (List<String>) map.get("authorities");
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("principal",principal);
        jsonObject.put("authorities",authorities);
        //给header里面添加值
        String base64 = Base64.encodeBase64String(jsonObject.toJSONString().getBytes());
        ServerHttpRequest tokenRequest = exchange.getRequest().mutate().header("json-token", base64).build();
        ServerWebExchange build = exchange.mutate().request(tokenRequest).build();
        return chain.filter(build);*/
        return chain.filter(exchange);
    }


    private Mono<Void> errorBackCall(ServerHttpResponse  response,int code,String message){
        JSONObject json = new JSONObject();
        json.put("status",code);
        json.put("data", message);
        byte[] bits = json.toJSONString().getBytes(StandardCharsets.UTF_8);
        DataBuffer buffer = response.bufferFactory().wrap(bits);
        //指定编码，否则在浏览器中会中文乱码
        response.getHeaders().add("Content-Type", "text/plain;charset=UTF-8");
        return response.writeWith(Mono.just(buffer));
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
