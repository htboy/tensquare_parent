package com.bi.cloud.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;


@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebFluxSecurity //由于Gateway使用的是WebFlux，所以需要使用@EnableWebFluxSecurity注解开启
public class WebGateWaySecurityConfig {

    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity httpSecurity) {
        httpSecurity
                .authorizeExchange()
                .pathMatchers("/**").permitAll()
                .anyExchange().permitAll()
                .and()
                .csrf()
                .disable()
                .cors()
                .and()
                .formLogin()
                .disable()
                .httpBasic().disable()
                .logout().disable()
                ;
        return httpSecurity.build();
    }


}
