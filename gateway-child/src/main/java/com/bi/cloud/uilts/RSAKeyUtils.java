package com.bi.cloud.uilts;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class RSAKeyUtils {
    /**
     * 非对称加密公钥Key
     */
    public static String getPubKey(String pubPath) throws Exception {
        InputStream in   = null;
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        try {
            URL url = new URL(pubPath);
            URLConnection conn = url.openConnection();
            in = conn.getInputStream();
            reader = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            return  sb.toString();
        }catch (Exception e){
            throw new Exception("读取公钥失败");
        }finally {
            if(null != reader){
                reader.close();
            }
            if(null != in){
                in.close();
            }
        }
    }
}
