package com.bi.cloud.config;

import com.bi.cloud.handler.AuthenticationFailhandlerEntryPoint;
import com.bi.cloud.handler.UnAccessDeniedHandler;
import com.bi.cloud.service.AuthUserService;
import com.bi.cloud.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;

@Configuration
@EnableWebSecurity(debug = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Lazy
    @Autowired
    private AuthUserService userService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /**
         * Spring Security 有：
         * HttpBasic模式登录认证、
         * formLogin模式登录认证
         * 两种模式，Basic模式比较简单也不那么安全，不建议在生产环境使用，但是可用来演示
         */
        http.authorizeRequests()
                .antMatchers("/oauth/**","/login","/logout","/favicon.ico")
                .permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .permitAll()
                .and()
                .httpBasic()
                .disable()
                .csrf()
                .disable()
                //因为使用JWT，所以不需要HttpSession：：放开这一句导致登入不了
              //  .sessionManagement().sessionCreationPolicy( SessionCreationPolicy.STATELESS)
                ;
    }
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
    }

    @Bean("passwordEncoder")
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    /*@Override
    public void configure(WebSecurity web) {
        //将项目中静态资源路径[开放]出来,前后端离不需要，但是需要另外设置跨越请求
        web.ignoring().antMatchers( "/css/**", "/fonts/**", "/img/**", "/js/**");
    }*/

}
