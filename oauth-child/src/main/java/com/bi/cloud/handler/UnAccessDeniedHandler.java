package com.bi.cloud.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * 无权限访问异常
 */
public class UnAccessDeniedHandler implements AccessDeniedHandler {


    private  final  static Logger log = LoggerFactory.getLogger(UnAccessDeniedHandler. class);




    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.setStatus(HttpStatus.FORBIDDEN.value());
        response.setHeader("Content-Type", "application/json;charset=UTF-8");
        String body = "{\"code\":403,\"msg\":\"权限不足\"}";
        response.getWriter().write(body);

    }
}
