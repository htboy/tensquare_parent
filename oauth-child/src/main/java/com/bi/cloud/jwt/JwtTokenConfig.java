package com.bi.cloud.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.UrlResource;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import java.net.MalformedURLException;


@Configuration
public class JwtTokenConfig {

    @Value("${privateKey}")
    private String privateKey;

    @Value("${password}")
    private String password;

    @Value("${alias}")
    private String alias;



    @Bean
    public TokenStore jwtTokenStore() throws MalformedURLException {
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() throws MalformedURLException {
        JwtAccessTokenConverter accessTokenConverter = new JwtAccessTokenConverter();
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(
                new UrlResource(privateKey),
                password.toCharArray());
        accessTokenConverter.setKeyPair(keyStoreKeyFactory.getKeyPair(alias));
       // accessTokenConverter.setSigningKey("admin-sign");
        return accessTokenConverter;
    }
}

